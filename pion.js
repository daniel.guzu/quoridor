var Pion = {
  color: '',
  size: 30,
  position:'',
}
var pion2 = Object.create(Pion);
var pion1 = Object.create(Pion);
pion2.color = "#9AF741";
pion1.color = "#4ACBFF";
pion2.position = [4,0];
pion1.position = [4,8];

function displayPion(pion) {
  fill(pion.color);
  ellipse(pion.position[0]*50+300, pion.position[1]*50+100, pion.size, pion.size);

  fill("white");
  ellipse(pion.position[0]*50+300, pion.position[1]*50+100, pion.size - 10, pion.size - 10);

  fill(pion.color);
  ellipse(pion.position[0]*50+300, pion.position[1]*50+100, pion.size - 18, pion.size - 18);
}


var arrayStroke = [];

function pion(p){
  if(turnMove ==true){

  allowMove(p.position[0],p.position[1]);

  if(mouseIsPressed && mouseButton==LEFT && mouseX>280 && mouseY>80 && mouseX<720 && mouseY<520){
    if(matrix[Math.floor((mouseX-280)/50)][Math.floor((mouseY-80)/50)].strokeAllow==true)
      p.position=[Math.floor((mouseX-280)/50),Math.floor((mouseY-80)/50)];

///eliminam toate Strokurile
      for(let x=0;x<9;x++){
        for(let y=0;y<9;y++){
          matrix[x][y].strokeAllow=false;
        }}

      turnMove = false;
  }
}                       
}

function allowMove(i,j){

  //caz 1
  if(i==0 && j==0){
        matrix[i+1][j].strokeAllow=true;
        matrix[i][j+1].strokeAllow=true;
          
  }
  else
  //caz 2
  if(i==8 && j==0){
        matrix[i-1][j].strokeAllow=true;
        matrix[i][j+1].strokeAllow=true;
  }
  else
  //caz 3
  if(i==0 && j==8){
        matrix[i][j-1].strokeAllow=true;
        matrix[i+1][j].strokeAllow=true;
  }
  else
  //caz 4
  if(i==8 && j==8){
        matrix[i][j-1].strokeAllow=true;
        matrix[i-1][j].strokeAllow=true;
  }
  else
  //caz 5
  if(i==0 && j>0 && j<8){
    matrix[i+1][j].strokeAllow=true;
    matrix[i][j+1].strokeAllow=true;
    matrix[i][j-1].strokeAllow=true;
  }
  else
  //caz 6
  if(i==8 && j>0 && j<8){
    matrix[i-1][j].strokeAllow=true;
    matrix[i][j+1].strokeAllow=true;
    matrix[i][j-1].strokeAllow=true;
  }
  else
  //caz 7
  if(j==0 && i>0 && i<8){
    matrix[i][j+1].strokeAllow=true;
    matrix[i+1][j].strokeAllow=true;
    matrix[i-1][j].strokeAllow=true;
  }
  else
  //caz 8
  if(j==8 && i>0 && i<8){
    matrix[i][j-1].strokeAllow=true;
    matrix[i+1][j].strokeAllow=true;
    matrix[i-1][j].strokeAllow=true;
  }
  else
  //caz 9
  if(i>0 && i<8 && j>0 && j<8){
    matrix[i][j-1].strokeAllow=true;
    matrix[i][j+1].strokeAllow=true;
    matrix[i+1][j].strokeAllow=true;
    matrix[i-1][j].strokeAllow=true;
  }

}