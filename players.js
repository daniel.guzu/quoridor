let valueWall_1 = 'gray';
let valueMove_1 = 'gray';
let valueWall_2 = 'gray';
let valueMove_2 = 'gray';

let punereWall = 0;
let turnWall = false;
let turnPlayer = 1;

function player1Chenar() {


     stroke('black');
     strokeWeight(5);
     fill('#9AF741');
     rect(0, 10, 190, 200);

     fill('#ffffff')
     rect(10, 30, 150, 30);
     fill('black');
     textSize(20);
     strokeWeight(1);
     /// text(input1.value(),20,52);
     fill(valueWall_1);
     let a = rect(45, 100, 100, 30);
     fill('black');
     text("Wall", 75, 121);
     fill(valueMove_1);
     let b = rect(45, 150, 100, 30);
     fill('black');
     text("Move", 75, 171);
     noStroke();

     if (turnPlayer == 1) {
          ///culoarea butonului Wall
          if (mouseX >= 45 && mouseX <= 145 && mouseY <= 130 && mouseY >= 100)
               valueWall_1 = '#BFC4C6';
          else
               valueWall_1 = 'gray';

          ///culoarea butonului Move
          if (mouseX >= 45 && mouseX <= 145 && mouseY <= 180 && mouseY >= 150)
               valueMove_1 = '#BFC4C6';
          else
               valueMove_1 = 'gray';

          ///Zidurile
          if (wallsPlayer1.length > 0) {
               if (mouseIsPressed && mouseButton == LEFT && mouseX > 45 && mouseY > 100 && mouseX < 145 && mouseY < 130) {
                    if (wallsPlayer1.length > 0)
                         turnWall = true;
               }
               if (punereWall == 1) {
                    wallsPlayer1.pop();
                    punereWall = 0;
                    turnPlayer = 2;
               }
          }
          ///Mutare


     }
}

function player2Chenar() {


     stroke('black');
     strokeWeight(5);
     fill('#4ACBFF');
     rect(810, 10, 190, 200);

     fill('#ffffff')
     rect(820, 30, 150, 30);

     fill('black');
     textSize(20);
     strokeWeight(1);
     /// text(input2.value(),830,52);
     fill(valueWall_2);
     let a = rect(855, 100, 100, 30);
     fill('black');
     text("Wall", 885, 121);
     fill(valueMove_2);
     let b = rect(855, 150, 100, 30);
     fill('black');
     text("Move", 885, 171);
     noStroke();

     if (turnPlayer == 2) {
          ///culoarea butonului Wall
          if (mouseX >= 855 && mouseX <= 955 && mouseY <= 130 && mouseY >= 100)
               valueWall_2 = '#BFC4C6';
          else
               valueWall_2 = 'gray';

          ///culoarea butonului Move
          if (mouseX >= 855 && mouseX <= 955 && mouseY <= 180 && mouseY >= 150)
               valueMove_2 = '#BFC4C6';
          else
               valueMove_2 = 'gray';

          //Zidurile
          if (wallsPlayer2.length > 0) {
               if (mouseIsPressed && mouseButton == LEFT && mouseX > 855 && mouseY > 100 && mouseX < 955 && mouseY < 130) {

                    turnWall = true;
               }

               if (punereWall == 1) {
                    wallsPlayer2.pop();
                    punereWall = 0;
                    turnPlayer = 1;
               }
          }
          //Mutare
     }
}