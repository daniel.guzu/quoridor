var Cell = {

  color: 'black',
  size: 40,
  position: '',
  strokeAllow: false,
  colorStroke: '',
};

var matrix = [];

for (let i = 0; i < 9; i++) {
  matrix[i] = new Array(9);
  for (let j = 0; j < 9; j++) {
    let cell = Object.create(Cell);
    cell.position = [i, j];
    matrix[i][j] = cell;
  }
}

class Table {

  static matrixQuoridor() {

    //culoarea tablei
    fill("#6D071A");
    square(200, 0, 600);

    ///piesele mijloc

    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        if (matrix[i][j].strokeAllow == true)
        stroke('gray')
        fill(matrix[i][j].color);
        square(280 + matrix[i][j].position[0] * 50, 80 + matrix[i][j].position[1] * 50, matrix[i][j].size);
        noStroke();
      }
    }

  }


  static displayTable() {

    ///dungile lungi
    fill("#87736C");
    rect(270, 65, 460, 10);
    rect(270, 525, 460, 10);

    ///marginile
    fill('black');
    for (let i = 0; i < 9; i++) {
      rect(280 + i * 40 + i * 10, 5, 40, 55);
      rect(280 + i * 40 + i * 10, 540, 40, 55);
    }
    triangle(205, 5, 270, 60, 270, 5);
    triangle(730, 5, 795, 5, 730, 60);
    triangle(205, 595, 270, 595, 270, 540);
    triangle(795, 595, 730, 595, 730, 540);
  }


  // zidurile nepuse pe tabla
  static walls() {

    ///zidurile de sus
    fill('yellow');
    for (let i = 0; i < 10; i++)
      if (wallsPlayer1[i]) {
        rect(270 + i * 40 + i * 10, 0, 10, 65);
      }

    ///zidurile de jos
    for (let i = 0; i < 10; i++)
      if (wallsPlayer2[i]) {
        rect(270 + i * 40 + i * 10, 535, 10, 65);
      }
  }
}