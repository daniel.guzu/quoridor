let start = false;
var finish = false;
var castigator = '';

function setup() {
  createCanvas(1000, 600);

  input1 = createInput();
  input1.position(400,300);
  input1.attribute('placeHolder',"Player 1");

  input2 = createInput();
  input2.position(770,300);
  input2.attribute('placeHolder',"Player 2");

  buttonStart =  createButton('START');
  buttonStart.position(630,350);
  buttonStart.mousePressed(startGame);

  fill('white');
  textSize(150);
  text('Quoridor',210,150);
  
}

function draw() {
  if(start == true)
  {
  clear();
  Table.matrixQuoridor();
  Table.displayTable();
  Table.walls();
  player1Chenar();
  player2Chenar();
  if(turnPlayer == 2){
  pion(pion2);}
  else
  if(turnPlayer == 1){
  pion(pion1);}
  displayPion(pion1);
  displayPion(pion2);
  wall();
  }

// if(finish == true)
// {
//   start = false;
//   fill('white');
//   textSize(100);
// text(castigator,400,200);
//}
}

function startGame() {
  if(input1.value().length >12 || input2.value().length>12)
  alert("Nume prea lung, Introduceti un nume mai scurt !!");
  else
  if(input1.value()!='' && input2.value()!=''){
  input1.remove();
  input2.remove();
  buttonStart.remove();
  start = true;
  }
}