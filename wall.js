var wallsPlayer1 = [];
var wallsPlayer2 = [];
var matrixWall = [];

var Wall = {
        position: ['', ''],
        orientation: "",
        allowVertical: true,
        allowHorizontal: true,
        put: false,
}



/// introducem zidurile in array-ul playerilor
for (let i = 0; i < 10; i++) {
        let x = Object.create(Wall);
        wallsPlayer1.push(x);
        let y = Object.create(Wall);
        wallsPlayer2.push(y);
}

/// introducem zidurile in matricea de ziduri
for (let i = 0; i < 8; i++) {
        matrixWall[i] = [];
        for (let j = 0; j < 8; j++) {
                let z = Object.create(Wall);
                z.position = [i, j];
                matrixWall[i].push(z);
        }
}

var keyPress = 'vertical';

function keyPressed() {

        if (keyCode === LEFT_ARROW) {
                keyPress = 'horizontal';
        } else if (keyCode === RIGHT_ARROW) {
                keyPress = 'vertical';
        }
}

function wall() {

        putWall();
        if(turnWall == true){
        for (let jWall = 0; jWall < 8; jWall++) {
                for (let iWall = 0; iWall < 8; iWall++) {
                        if ((matrixWall[iWall][jWall].allowVertical == true && keyPress == "vertical") 
                        || (matrixWall[iWall][jWall].allowHorizontal == true && keyPress == "horizontal")) {
                                if (mouseX > (300 + 50 * iWall) && mouseX < (350 + 50 * iWall) && mouseY > (100 + 50 * jWall) && mouseY < (150 + 50 * jWall)) {
                                        shadowWall(iWall, jWall,keyPress);

                                        if (mouseIsPressed && mouseButton == LEFT) {
                                                matrixWall[iWall][jWall].orientation = keyPress;
                                                matrixWall[iWall][jWall].position = [iWall, jWall];
                                                matrixWall[iWall][jWall].put = true;
                                                matrixWall[iWall][jWall].allowHorizontal=false;
                                                matrixWall[iWall][jWall].allowVertical=false;
                                                allowWall(matrixWall[iWall][jWall].position[0],matrixWall[iWall][jWall].position[1]);
                                                turnWall = false;
                                                punereWall = 1;

                                        }
                                }
                        }

                
        }}
}}

function putWall() {
        fill('yellow');
        for (let j = 0; j < 8; j++) {
                for (let i = 0; i < 8; i++) {
                        if (matrixWall[i][j].put == true) {
                                if (matrixWall[i][j].orientation == 'vertical')
                                        rect((320 + 50 * i), (80 + 50 * j), 10, 90);
                                else
                                        rect((280 + 50 * i), (120 + 50 * j), 90, 10);

                        }
                }
        }
}

function shadowWall(x, y, o) {

        colorMode(HSB, 255);
        let c = color('hsb(60, 100%, 25%)');
        fill(c);
        if (o == 'vertical')
                rect((320 + 50 * x), (80 + 50 * y), 10, 90);
        else
                rect((280 + 50 * x), (120 + 50 * y), 90, 10);

}

function allowWall(i, j) {
        
//caz 1
        if(i==0 && j==0){   
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j+1].allowVertical = false;
                }
                        else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;

                }
        }
        else
//caz 2
        if(i==0 && j==7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j-1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;
                }
        }
        else
//caz 3
        if(i==7 && j==0){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j+1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
        else
//caz 4
        if(i==7 && j==7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j-1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
//caz 5
        if(i==0 && j>0 && j<7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j-1].allowVertical = false;
                        matrixWall[i][j+1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;
                }
        }
//caz 6
        if(j==7 && i>0 && i<7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j-1].allowVertical = false;
                        
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
//caz 7
        if(i==7 && j>0 && j<7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j-1].allowVertical = false;
                        matrixWall[i][j+1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
//caz 8
        if(j==0 && i>0 && i<7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j+1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
//caz 9
        else
        if(i>0 && i<7 && j>0 && j<7){
                if(matrixWall[i][j].orientation=="vertical"){
                        matrixWall[i][j+1].allowVertical = false;
                        matrixWall[i][j-1].allowVertical = false;
                }
                else
                if(matrixWall[i][j].orientation=="horizontal"){
                        matrixWall[i+1][j].allowHorizontal = false;
                        matrixWall[i-1][j].allowHorizontal = false;
                }
        }
}